#ifndef MOTOR_H
#define MOTOR_H

#include <Arduino.h>

enum Action {
  NONE = 0,
  POWER = 1,
  DIRECTION = 2
};

class Motor
{
	public:
		Motor();
    
    /*
     * Parse input packet.
     * Return true of success and sets action and value
     */
    bool ParsePacket(char a_Packet[], Action& a_Action, int& a_Value);

  private:
  Action GetActionFromStr(String& a_Action);
};

#endif
