#include "Motor.h"

Motor::Motor()
{
	// empty
}

bool Motor::ParsePacket(char a_Packet[], Action& a_Action, int& a_Value)
{
  String packet = a_Packet;
  String action = packet.substring(0,2);
  String value = packet.substring(4);
  a_Action = GetActionFromStr(action);
  if (a_Action == NONE) {
    return false;
  }
  a_Value = value.toInt();
  
  return true;
}

Action Motor::GetActionFromStr(String& a_Action)
{
  if (a_Action == "POW") {
    return POWER;
  } else if (a_Action == "DIR") {
    return DIRECTION;
  } else {
    return NONE;
  }
}

