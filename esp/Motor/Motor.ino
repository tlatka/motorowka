#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include "Motor.h"

WiFiUDP Udp;
unsigned int localUdpPort = 12345;  // local port to listen on
char incomingPacket[255];  // buffer for incoming packets

Motor motor;

void setup()
{
  Serial.begin(115200);
  Serial.println();

  Serial.print("Setting soft-AP ... ");

  IPAddress netIp(192,168,4,2);
  IPAddress netGateway(192,168,4,1);
  IPAddress netSubnet(255,255,255,0);
  char netSsid[] = "Motor";
  char netPass[] = "kopytkoo";
  int netChannel = 10;
  boolean netHidden = false;

  if (WiFi.softAPConfig(netIp, netGateway, netSubnet) == true) {
    Serial.println("Conf Ready");
  } else {
    Serial.println("Conf Failed!");
  }
  
  if(WiFi.softAP(netSsid, netPass, netChannel, netHidden) == true) {
    Serial.println("AP Ready");
  } else {
    Serial.println("AP Failed!");
  }

  Udp.begin(localUdpPort);
  Serial.printf("Now listening at IP %s, UDP port %d\n", WiFi.localIP().toString().c_str(), localUdpPort);
}

void loop()
{
  int packetSize = Udp.parsePacket();
  if (packetSize) {
    // receive incoming UDP packets
    Serial.printf("Received %d bytes from %s, port %d\n", packetSize, Udp.remoteIP().toString().c_str(), Udp.remotePort());
    int len = Udp.read(incomingPacket, 255);
    if (len > 0) {
      incomingPacket[len] = 0;
    }
    Serial.printf("UDP packet contents: %s\n", incomingPacket);
    Action action;
    int value;
    if (!motor.ParsePacket(incomingPacket, action, value)) {
      Serial.printf("Failed to parse incoming packet\n");
      return;
    }
    Serial.printf("Action %d value %d\n", action, value);
    
  }
}
