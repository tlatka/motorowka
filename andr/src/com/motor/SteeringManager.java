package com.motor;

public class SteeringManager {
	
	SteeringManager()
	{
		m_PowerStatus = false;
	    m_Progress = 50;
	    m_Direction = 0;
	}
	
    private boolean m_PowerStatus = false;
    private int m_Progress;
    private int m_Direction;
   
    public String ChangePowerStatus()
    {
    	m_PowerStatus = !m_PowerStatus;
        String status;
        if (m_PowerStatus) {
            status = "ON";
        } else {
            status = "OFF";
        }
        return "power:" + status;
    }
    
    public int ProgressToDirection(int a_Progress)
    {
    	int minDirection = -90;
    	int maxDirection = 90;
    	int diff = maxDirection - minDirection;
    	m_Direction = minDirection + a_Progress * diff / 100;
    	
        return m_Direction;
    }
    
    public String GetDirectionMessage(int a_Progress)
    {
    	m_Progress = a_Progress;
        return "direction:" + a_Progress;
    }
}
