package com.motor;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import android.util.Log;

public class SendingManager {
    private String m_ServerIp = "192.168.4.2";
    private int m_ServerPort = 12345;
    private Thread m_UdpSendThread;
    
    private String m_UdpOutputData;
    private Boolean m_SendUdp = false;
    
    SendingManager() {
        InitSendThread();
        m_UdpSendThread.start();
    }
    
    public void SendText(String a_Message) {
        m_UdpOutputData = a_Message;
        m_SendUdp = true;
    }
    
    private void InitSendThread()
    {
        m_UdpSendThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(10);
                        } catch (InterruptedException e1) {
                            e1.printStackTrace();
                        }
                        if (m_SendUdp == true) {
                            try {
                                // get server name
                                InetAddress serverAddr = InetAddress.getByName(m_ServerIp);
                                Log.d("UDP", "C: Connecting...");

                                // create new UDP socket
                                DatagramSocket socket = new DatagramSocket();

                                // prepare data to be sent
                                byte[] buf = m_UdpOutputData.getBytes();

                                // create a UDP packet with data and its destination ip & port
                                DatagramPacket packet = new DatagramPacket(buf, buf.length, serverAddr, m_ServerPort);
                                Log.d("UDP", "C: Sending: '" + new String(buf) + "'");

                                // send the UDP packet
                                socket.send(packet);
                                socket.close();
                                Log.d("UDP", "C: Sent.");
                                Log.d("UDP", "C: Done.");
                                } 
                                catch (Exception e) {
                                    Log.e("UDP", "C: Error", e);
                                }
                            try {
                                Thread.sleep(10);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            m_SendUdp = false;
                            }
                    }
            }
        });
    }
    
}

