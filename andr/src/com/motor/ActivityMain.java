package com.motor;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;


public class ActivityMain extends Activity {

    private SendingManager m_SendManager = new SendingManager();
    private SteeringManager m_SteerManager = new SteeringManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        Button buttonPower = (Button) findViewById(R.id.buttonPower);
        final TextView powerStatusText = (TextView)findViewById(R.id.textViewPowerStatus);
        buttonPower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = m_SteerManager.ChangePowerStatus();
                m_SendManager.SendText(message);
                powerStatusText.setText(message);
            }
        });
        
       SeekBar seekBarWheel = (SeekBar)findViewById(R.id.seekBarDirection); 
       seekBarWheel.setProgress(50);
       final TextView wheelStatusText = (TextView)findViewById(R.id.textViewDirection);
       wheelStatusText.setText("0");
       seekBarWheel.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
           @Override 
           public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
               wheelStatusText.setText(String.valueOf(m_SteerManager.ProgressToDirection(progress)));
               String message = m_SteerManager.GetDirectionMessage(progress);
               m_SendManager.SendText(message);
           } 
    
           @Override 
           public void onStartTrackingTouch(SeekBar seekBar) {
           }
           @Override 
           public void onStopTrackingTouch(SeekBar seekBar) {
           } 
       });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
